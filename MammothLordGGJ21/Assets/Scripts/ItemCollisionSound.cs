﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollisionSound : MonoBehaviour
{

    private void OnCollisionEnter(Collision collision) {
        ItemCollisionSoundManager.instance.CheckTags(transform.position, gameObject.tag, collision.gameObject.tag);
    }
    
}
