﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoozeValueSlider : MonoBehaviour
{

    FloatVariable Booze;
    Slider mySlider;
    // Start is called before the first frame update
    void Start()
    {
        mySlider = GetComponent<Slider>();
        Booze = Resources.Load("BoozeMeter") as FloatVariable;
    }

    // Update is called once per frame
    void Update()
    {
        mySlider.value = Booze.Value/100;
    }
}
