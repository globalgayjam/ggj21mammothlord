﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollisionSoundManager : MonoBehaviour
{

    public static ItemCollisionSoundManager instance;
    [FMODUnity.EventRef]
    public  string bottlesColliding;
    [FMODUnity.EventRef]
    public  string cansColliding;
    [FMODUnity.EventRef]
    public  string bottleAndCanColliding;
    [FMODUnity.EventRef]
    public  string bottleAndFloorColliding;
    [FMODUnity.EventRef]
    public  string canAndFloorColliding;

    private  bool playingSound;

    private void Awake() {
        if (instance == null)
            instance = this;
        else Destroy(this);
    }

    public void CheckTags(Vector3 position, string tag ,string otherTag) {
        if (playingSound) return;
        playingSound = true;
        if (tag == "bottle" && otherTag == "bottle") {
            FMODUnity.RuntimeManager.PlayOneShot(bottlesColliding, position);
        } else if (tag == "bottle" && otherTag == "can") {
            FMODUnity.RuntimeManager.PlayOneShot(bottleAndCanColliding, position);
        } else if (tag == "can" && otherTag == "can") {
            FMODUnity.RuntimeManager.PlayOneShot(cansColliding, position);
        } else if (tag == "bottle" && otherTag == "floor") {
            FMODUnity.RuntimeManager.PlayOneShot(bottleAndFloorColliding, position);
        } else if (tag == "can" && otherTag == "floor") {
            FMODUnity.RuntimeManager.PlayOneShot(canAndFloorColliding, position);
        }
        StartCoroutine(WaitForNextFrame());
    }

    IEnumerator WaitForNextFrame() {
        yield return new WaitForEndOfFrame();
            playingSound = false;
    }


}
