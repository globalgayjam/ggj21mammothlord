﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using FMODUnity;

public class PhoneInterface : MonoBehaviour
{
    public static PhoneInterface instance;
    private PhoneNumbers PhoneNumbers;
    public Image phoneImage;
    public TextMeshProUGUI PhoneNumber;
    public bool OnPhone;
    [FMODUnity.EventRef]
    public string RingingAudio;

    [FMODUnity.EventRef]
    public string WrongNumber;

    [FMODUnity.EventRef]
    public string SatanNumber;

    FMOD.Studio.EventInstance AudioInstance;
    FMOD.Studio.EventInstance AudioInstance2;
    FMOD.Studio.EventInstance AudioInstance3;
    bool canExit;

    private void Awake() {
        instance = this;
    }

    private void Start() {
        PhoneNumbers = Resources.Load("PhoneNumbers") as PhoneNumbers;

        AudioInstance = FMODUnity.RuntimeManager.CreateInstance(RingingAudio);
        AudioInstance2 = FMODUnity.RuntimeManager.CreateInstance(WrongNumber);
        AudioInstance3 = FMODUnity.RuntimeManager.CreateInstance(SatanNumber);
    }
    private void Update() {
        if (OnPhone) {
            if (Input.GetMouseButtonDown(1) && canExit) {
                ClosePhone();
                PlayerMouseController.instance.StopUse();
            }
        }
    }

    public void OpenPhone() {
        phoneImage.gameObject.SetActive(true);
        OnPhone = true;
        canExit = false;
        StartCoroutine(wait());
        Debug.Log("open phone");
    }

    IEnumerator wait() {
        yield return new WaitForSeconds(0.1f);
        canExit = true;
    }

    public void ClosePhone() {
        phoneImage.gameObject.SetActive(false);
        PhoneNumber.text = "";
        AudioInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        AudioInstance2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        AudioInstance3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        OnPhone = false;
        canExit = false;
        StopAllCoroutines();
        Debug.Log("close phone");
    }

    public void PhoneInput(int Number) {
        if (Number<10)
        FMODUnity.RuntimeManager.PlayOneShot("event:/dial"+Number);
        if (Number == 10) {
            PhoneNumber.text = PhoneNumber.text.Remove(PhoneNumber.text.Length - 1); ;
        } else if (Number == 12) {
            AudioInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            AudioInstance2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            AudioInstance3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            StartCoroutine(CallPhone());
        }
        else {
            PhoneNumber.text += Number;
        }
    }
    bool noneFound;
    public IEnumerator CallPhone() {
        AudioInstance.start();
        float rand = Random.Range(2f, 5f);
        yield return new WaitForSeconds(rand);
        noneFound = true;
        AudioInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        AudioInstance2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        AudioInstance3.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);

        for (int i = 0; i < PhoneNumbers.PhoneNumberList.Count; i++) {
            if (PhoneNumber.text != "" && int.Parse(PhoneNumber.text) == PhoneNumbers.PhoneNumberList[i].Number && !PhoneNumbers.PhoneNumberList[i].Called) {
                FMODUnity.RuntimeManager.PlayOneShot(PhoneNumbers.PhoneNumberList[i].CorrectNumberAudio);
                PhoneNumbers.PhoneNumberList[i].Called = true;
                SpawnBandMember.instance.SpawnMember();
                noneFound = false;

            }
        }
        if (PhoneNumber.text != "" && int.Parse(PhoneNumber.text) == 666) {
            AudioInstance3.start();
        }
        if (PhoneNumber.text != "" && noneFound) {
            AudioInstance2.start();
        }

    }



}
