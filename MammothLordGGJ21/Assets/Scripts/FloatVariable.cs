﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Variables/FloatVariable")]
public class FloatVariable : ScriptableObject {


    public float Value;

    public void SetValue(float value) {
        Value = value;
        if (OnValueChanged != null) {
            OnValueChanged();
        }
    }

    public void SetValue(FloatVariable value) {
        Value = value.Value;
        if (OnValueChanged != null)
            OnValueChanged();
    }

    public void ApplyChange(float amount) {
        Value += amount;
        if (Value > 100) Value = 100;
        if (Value < 0) Value = 0;
        if (OnValueChanged != null)
            OnValueChanged();
    }

    public void ApplyChange(FloatVariable amount) {
        Value += amount.Value;
        if (OnValueChanged != null)
            OnValueChanged();
    }


    public event Action OnValueChanged; // actually means on value decreased

}

