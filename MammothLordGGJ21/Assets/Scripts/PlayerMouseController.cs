﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMouseController : MonoBehaviour {

    public Animator RockStar;
    public static PlayerMouseController instance;
    public LayerMask mask;
    public LayerMask mask2;
    public Transform AnchorPoint;
    public float range;
    public static bool UsingItem;
    public Collider MyCollider;
    //public Collider MyCollider2;
    public static GameObject ItemInHand;
    bool released;
    float forceAmount;
    Vector3 dir;
    Vector3 previusPos;
    public bool Animating;
    Quaternion RotAtStart;
    Vector3 PosAtStart;
    [FMODUnity.EventRef]
    public string vomitAudio;
    [FMODUnity.EventRef]
    public string eatAudio;
    [FMODUnity.EventRef]
    public string drinkAudio;
    [FMODUnity.EventRef]
    public string drinkAudio2;
    [FMODUnity.EventRef]
    public string randomAudio0;
    [FMODUnity.EventRef]
    public string randomAudio1;
    [FMODUnity.EventRef]
    public string randomAudio2;

    float randomAudioTimer;

    private void Awake() {
        instance = this;

    }
    private void Start() {
        RotAtStart = transform.rotation;
        PosAtStart = transform.position;
    }

    private void Update() {
        if (Animating) return;
        if (!UsingItem) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (ItemInHand != null) {
                Physics.Raycast(ray, out hit, 100, mask, QueryTriggerInteraction.Ignore);
            } else {
                Physics.Raycast(ray, out hit, 100, mask2, QueryTriggerInteraction.Ignore);
            }
            dir = hit.point - AnchorPoint.position;
            if (Vector3.Distance(AnchorPoint.position, hit.point) < range) {
                transform.position = hit.point + new Vector3(0, 0.05f, 0);
            } else {
                transform.position = AnchorPoint.position + dir.normalized * range + new Vector3(0, 0.05f, 0);
            }
            if ((Input.GetMouseButton(0) || UsingItem) && ItemInHand != null) {
                MyCollider.enabled = false;
               // MyCollider2.enabled = false;

            } else {
                if (!released)
                    StartCoroutine(EnableCollision());
            }
        }
        if (Input.GetMouseButtonUp(0) && ItemInHand != null && !UsingItem) {
            Debug.Log("normal drop item");
            ItemInHand.GetComponent<Rigidbody>().useGravity = true;
            ItemInHand.GetComponent<Rigidbody>().isKinematic = false;
            ItemInHand.GetComponent<Rigidbody>().AddForce(dir.normalized*(Vector3.Distance(transform.position,previusPos))*20,ForceMode.Impulse);
            ItemInHand = null;
        }
        if (Vector3.Distance(transform.position, previusPos)>2.5f && canVomit) {
            Animate(1);
        }

        if(Time.time> randomAudioTimer) {
            randomAudioTimer = Time.time + Random.Range(6, 12f);
            int randInt = Random.Range(0, 3);
            if (randInt == 0)
            FMODUnity.RuntimeManager.PlayOneShot(randomAudio0,transform.position);
            if (randInt == 1)
                FMODUnity.RuntimeManager.PlayOneShot(randomAudio1, transform.position);
            if (randInt == 2)
                FMODUnity.RuntimeManager.PlayOneShot(randomAudio2, transform.position);
        }

        previusPos = transform.position;
    }

    public void Animate(int Animation, int bottle = 0) {
        if (Animation == 0) {
            transform.position = PosAtStart;
            transform.rotation = RotAtStart;
            //RockStar.SetInteger("animInt", 0);
            RockStar.SetTrigger("drink");
            if (drinkAudio != "" && bottle == 0)
                FMODUnity.RuntimeManager.PlayOneShot(drinkAudio);
            if (drinkAudio != "" && bottle == 1)
                FMODUnity.RuntimeManager.PlayOneShot(drinkAudio2);
            Animating = true;
        }
        if (Animation == 2) {
            transform.position = PosAtStart;
            transform.rotation = RotAtStart;
            //RockStar.SetInteger("animInt", 0);
            RockStar.SetTrigger("drink");
            if (eatAudio != "")
                FMODUnity.RuntimeManager.PlayOneShot(eatAudio);
            Animating = true;
        }
        if (Animation == 1) {
            StopUse();
            transform.position = PosAtStart;
            transform.rotation = RotAtStart;
            //RockStar.SetInteger("animInt", 0);
            RockStar.SetTrigger("vomit");
            FMODUnity.RuntimeManager.PlayOneShot(vomitAudio);
            Animating = true;
            canVomit = false;
            StartCoroutine(VomitCD());
        }
        if (Animation == 3) {
            StopUse();
            transform.position = PosAtStart;
            transform.rotation = RotAtStart;
            //RockStar.SetInteger("animInt", 0);
            RockStar.SetTrigger("phone");
            Animating = true;
        }
        if (Animation == 4) {
            StopUse();
            transform.position = PosAtStart;
            transform.rotation = RotAtStart;
            //RockStar.SetInteger("animInt", 0);
            RockStar.SetTrigger("sleep");
            Animating = true;
        }
    }

    IEnumerator VomitCD() {
        yield return new WaitForSeconds(4f);
        canVomit = true;
    }
    bool canVomit = true;
    public void StopUse() {
        if (ItemInHand) {
            Debug.Log("hand stop use");
            ItemInHand.GetComponent<Rigidbody>().useGravity = true;
            ItemInHand.GetComponent<Rigidbody>().isKinematic = false;
            ItemInHand.GetComponent<GrabbableItem>().StopUse();
            ItemInHand = null;
        }
    }

    public void StopAnimate() {
        Animating = false;
        StopUse();
    }
    

    IEnumerator EnableCollision() {
        released = true;
        yield return new WaitForSeconds(1f);
        MyCollider.enabled = true;
       // MyCollider2.enabled = true;
        released = false;
    }
}
