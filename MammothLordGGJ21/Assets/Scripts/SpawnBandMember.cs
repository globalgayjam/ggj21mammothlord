﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class SpawnBandMember : MonoBehaviour
{
    public static SpawnBandMember instance;

    int currentBandMembers = 0;
    public GameObject[] Members;
    public Transform[] Positions;
    [FMODUnity.ParamRef]
    public string ParameterName;
    private FMOD.Studio.PARAMETER_DESCRIPTION parameterDescription;

    private void Awake() {
        instance = this;
    }
    private void Start() {
        RuntimeManager.StudioSystem.getParameterDescriptionByName(ParameterName, out parameterDescription);
        for (int i = 0; i < Members.Length; i++) {
            Members[i].SetActive(false);
        }
    }

    public void Restart() {
        currentBandMembers = 0;
        RuntimeManager.StudioSystem.setParameterByID(parameterDescription.id, currentBandMembers);
        for (int i = 0; i < Members.Length; i++) {
            Members[i].SetActive(false);
        }
    }

    public void SpawnMember() {


        StartCoroutine(Delay());
    }

    IEnumerator Delay() {
        float rand = Random.Range(8f, 15f);
        yield return new WaitForSeconds(rand);
        Members[currentBandMembers].SetActive(true);
        RuntimeManager.StudioSystem.setParameterByID(parameterDescription.id, currentBandMembers+1);
        //Instantiate(Members[currentBandMembers], Positions[currentBandMembers]);
        currentBandMembers++;
        if (currentBandMembers > 3) {
            RuntimeManager.StudioSystem.setParameterByID(parameterDescription.id, 5);
            GameManager.instance.GameWin();
        }
    }
}
