﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using FMODUnity;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public float GameEndTime;
    float currentTime;
    public bool timerOn;
    bool win;
    public TextMeshProUGUI text;
    private PhoneNumbers PhoneNumbers;
    public GameObject tutorial;
    public GameObject winText;
    public GameObject startMenu;
    [EventRef]
    public string musicAudio;
    [EventRef]
    public string Ringing;
    [EventRef]
    public string Snore;
    [EventRef]
    public string AnswerToProducer;
    FMOD.Studio.EventInstance AudioInstance;
    FMOD.Studio.EventInstance AudioInstance2;
    FMOD.Studio.EventInstance Music;
    private void Awake() {
        instance = this;
    }

    private void Start() {
        Music = FMODUnity.RuntimeManager.CreateInstance(musicAudio);
        AudioInstance2 = FMODUnity.RuntimeManager.CreateInstance(Snore);
        AudioInstance2.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
        AudioInstance2.start();
        AudioInstance = FMODUnity.RuntimeManager.CreateInstance(Ringing);
        AudioInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
        AudioInstance.start();
        // StartGame();
    }

    public void StartGame() {
        Debug.Log("start");
        PhoneNumbers = Resources.Load("PhoneNumbers") as PhoneNumbers;
        RockstarNeedsManager.instance.Restart();
        tutorial.SetActive(true);
        startMenu.SetActive(false);
        PhoneNumbers.givenNumbers = -1;
        GenerateNumbers();
        SpawnBandMember.instance.Restart();
        PlayerMouseController.instance.Animate(3);
        AudioInstance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        AudioInstance2.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
        RuntimeManager.PlayOneShot(AnswerToProducer);
        Music.start();
        StartTimer();
    }
    int LowRange = 1000;
    int HighRange = 2000;
    private void GenerateNumbers() {
        LowRange = 1000;
        HighRange = 2000;
        for (int i = 0; i < PhoneNumbers.PhoneNumberList.Count; i++) {
            PhoneNumbers.PhoneNumberList[i].Number = Random.Range(LowRange, HighRange);
            PhoneNumbers.PhoneNumberList[i].Called = false;
            LowRange += 1000;
            HighRange += 1000;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timerOn) {
            currentTime -= Time.deltaTime;
            text.text = FormatTimeSeconds(currentTime);
            if (currentTime<= 0) {
                GameOver();
            }
        }
    }
    [ContextMenu("start")]
    public void StartTimer() {
        timerOn = true;
        currentTime = GameEndTime;
    }

    public void GameWin() {
        timerOn = false;
        tutorial.SetActive(false);
        winText.SetActive(true);
    }

    public void GameOver() {
        timerOn = false;
        tutorial.SetActive(false);
        PlayerMouseController.instance.Animate(4);
        AudioInstance2 = FMODUnity.RuntimeManager.CreateInstance(Snore);
        AudioInstance2.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
        AudioInstance2.start();
        AudioInstance = FMODUnity.RuntimeManager.CreateInstance(Ringing);
        AudioInstance.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(transform));
        AudioInstance.start();
        startMenu.SetActive(true);
        Music.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
    }


    string FormatTimeMilliseconds(float time) {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        int milliseconds = (int)(1000 * (time - minutes * 60 - seconds));
        return string.Format("{0:00}:{1:00}:{2:000}", minutes, seconds, milliseconds);
    }
    string FormatTimeSeconds(float time) {
        int minutes = (int)time / 60;
        int seconds = (int)time - 60 * minutes;
        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
