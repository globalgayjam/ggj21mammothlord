﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GrabbableItem : MonoBehaviour
{
    public Renderer MyRenderer;
    public int HighlightShader;
    private GameObject theHand;
    public Rigidbody RB;
    private bool stoppedUse;
    public bool IsPhone;
    public bool IsFood;
    public bool IsDrink;
    public bool IsNumber;
    public Sprite NumberBackground;
    public int bottle;
    private void Start() {
       HighlightShader = Shader.PropertyToID("highlight");
    }

    private void Update() {
        if (PlayerMouseController.ItemInHand == this.gameObject) {

            transform.position = theHand.transform.position + new Vector3(0, 0, 0.15f);
            transform.eulerAngles = theHand.transform.eulerAngles;


            if (!PlayerMouseController.UsingItem && Input.GetMouseButtonDown(1) && (!IsPhone || (IsPhone && RockstarNeedsManager.instance.CanUsePhone))) {
                CheckMyEvent();
                PlayerMouseController.UsingItem = true;
                Debug.Log("use item");
            }
        }
    }

    public void StopUse() {
        RB.isKinematic = false;
        RB.useGravity = true;
        CheckMyStopEvent();
        PlayerMouseController.UsingItem = false;
        Debug.Log("item stop use");
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag == "Player") {
            //   MyRenderer.material.SetFloat(HighlightShader, 1);
            if (Input.GetMouseButtonDown(0) && PlayerMouseController.ItemInHand == null) {
                PlayerMouseController.ItemInHand = this.gameObject;
                theHand = other.gameObject;
                RB.isKinematic = true;
                RB.useGravity = false;
            }


        }
    }

    private void CheckMyEvent() {
        if (IsPhone) {
            PhoneInterface.instance.OpenPhone();
        } else if (IsFood) {
            RockstarNeedsManager.instance.Eat(Random.Range(5, 20));
            PlayerMouseController.instance.Animate(2);
        } else if (IsDrink) {
            RockstarNeedsManager.instance.Drink(Random.Range(1, 10));
            PlayerMouseController.instance.Animate(0, bottle);
        }else if (IsNumber) {
            PhoneNumbers phoneNumbers = Resources.Load("PhoneNumbers") as PhoneNumbers;
            InspectInterface.instance.Show(phoneNumbers.GetNumber());
        }
    }

    private void CheckMyStopEvent() {
        if (IsPhone) {
            PhoneInterface.instance.ClosePhone();
        } else if (IsFood) {

        } else if (IsDrink) {

        } else if (IsNumber) {

        }
    }

    private void OnTriggerExit(Collider other) {
       // if (other.tag == "Player")
           // MyRenderer.material.SetFloat(HighlightShader, 0);
    }
}
