﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockstarNeedsManager : MonoBehaviour
{
    public static RockstarNeedsManager instance;
    FloatVariable Food;
    FloatVariable Booze;
    public float DecreaseAmount;
    public float DecreaseTime;
    public bool CanUsePhone = true;
    private float nextTime;
    
    bool restarted;
    // Start is called before the first frame update
    private void Awake() {
        instance = this;
    }

    void Start()
    {
        Food = Resources.Load("FoodMeter") as FloatVariable;
        Booze = Resources.Load("BoozeMeter") as FloatVariable;
    }

    public void Restart() {
        nextTime = Time.time + DecreaseTime;
        Food.SetValue(15);
        Booze.SetValue(15);
        CanUsePhone = true;
        restarted = true;
    }

    private void Update() {
        if (!GameManager.instance.timerOn) return;
        if (Time.time >= nextTime ) {
            nextTime += DecreaseTime;
            Food.ApplyChange(-DecreaseAmount);
            Booze.ApplyChange(-DecreaseAmount);
        }
        if ((Food.Value<= 0 || Booze.Value <= 0) && restarted) {
            //CanUsePhone = false;
            GameManager.instance.GameOver();
            restarted = false;
        }
        //else {
        //    CanUsePhone = true;
        //}
    }

    public void Eat(float value) {
        Food.ApplyChange(value);
    }

    public void Drink(float value) {
        Booze.ApplyChange(value);
    }
}
