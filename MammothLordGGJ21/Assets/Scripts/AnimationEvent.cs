﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationEvent : MonoBehaviour
{
    public ParticleSystem VomitPS;

    public void AnimationEnd() {
        PlayerMouseController.instance.StopAnimate();
        Debug.Log("stopped animating");
    }
    public void Vomit() {
        VomitPS.Play();
    }
}
