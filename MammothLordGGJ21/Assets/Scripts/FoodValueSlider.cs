﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FoodValueSlider : MonoBehaviour
{
    FloatVariable Food;
    Slider mySlider;
    // Start is called before the first frame update
    void Start() {
        mySlider = GetComponent<Slider>();
        Food = Resources.Load("FoodMeter") as FloatVariable;
    }

    // Update is called once per frame
    void Update() {
        mySlider.value = Food.Value/100;
    }
}
