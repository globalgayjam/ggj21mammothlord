﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        SceneManager.LoadScene("Background", LoadSceneMode.Additive);
    }

    public void Restart()
    {
        SceneManager.LoadScene("SampleScene 1");
    }

}
