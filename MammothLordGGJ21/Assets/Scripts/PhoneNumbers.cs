﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu]
public class PhoneNumbers : ScriptableObject
{
    public List<APhoneNumber> PhoneNumberList = new List<APhoneNumber>();
    public int givenNumbers = -1;

    public int GetNumber() {
        if (givenNumbers+1 <= PhoneNumberList.Count) {
            givenNumbers++;
            return PhoneNumberList[givenNumbers].Number;
        } else return Random.Range(9000,10000);
    }
   
}

[System.Serializable]
public class APhoneNumber {
    public int Number;
    public bool CorrectNumber = true;
    public bool Called;
    [FMODUnity.EventRef]
    public string CorrectNumberAudio;
}
