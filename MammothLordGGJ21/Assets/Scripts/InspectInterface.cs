﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InspectInterface : MonoBehaviour
{
    public static InspectInterface instance;
    public GameObject Panel;
    public Image NumberImage;
    public TextMeshProUGUI text;
    public bool Inspecting;
    bool canExit;
    private void Awake() {
        instance = this;
    }

    public void Show(int Number) {
        //NumberImage.sprite = sprite;
        text.text = "" + Number;
        Panel.SetActive(true);
        Inspecting = true;
        StartCoroutine(wait());
    }

    IEnumerator wait() {
        yield return new WaitForSeconds(0.1f);
        canExit = true;
    }
    public void Hide() {
        Panel.SetActive(false);
        Inspecting = false;
        canExit = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Inspecting) {
            if (Input.GetMouseButtonDown(1) && canExit) {
                Hide();
                PlayerMouseController.instance.StopUse();
            }
        }
    }
}
